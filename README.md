# Getting Started with GitOps with Flux and Gitlab
Learn how to use the GitOps methodology to simplify your Kubernetes deployments.

![alt text](images/flux.png "Flux")

GitOps is a Kubernetes application delivery methodology. It aims to simplify the deployment and operation of Kubernetes applications.

In this article we will use Flux, which can be installed as a Kubernetes operator.

The Flux operatorkeeps the cluster state and a repository in sync. Any configuration change made in the repository is automatically applied to the Kubernetes cluster.

In this guide we will set up Flux and deploy a demo application via the Git repository.

## Prerequisites

To get started, we need ```kubectl``` connected to a Kubernetes cluster and a Git repository in Gitlab.


## Install fluxctl

```fluxctl``` is a command line tool to interact with Flux. On macOS you can install ```fluxctl``` with Homebrew.

```sh
brew install fluxctl
```

You can find installation instructions for other platforms here: https://docs.fluxcd.io/en/1.17.1/references/fluxctl.html

## Create a Kubernetes namespace for Flux

Let’s start by creating a namespace for the Flux operator.

```sh
kubectl create namespace flux
```

## Generate Flux manifest files

We use the fluxctl tool to generate the Kubernetes manifest files for the Flux operator. You need to specify the repository url, Git user information. You also need to provide the paths to the folders where the Kubernetes manifests are kept and the namespace where the operator should be deployed.

```sh
fluxctl install  \
--git-url=git@gitlab.com:organicnz/gitops-flux-k8s \
--git-user=flux \
--git-email=flux@tomcode.com \
--git-path=namespaces,workloads \
--namespace=flux > flux.yaml
```

## Apply manifest files

Let’s apply the Flux operator using kubectl.

```sh
kubectl apply -f flux.yaml
```

## Verify deployment

Verify the deployment by checking the status Kubernetes pods in the flux namespace. There should be the Flux operator as well as a Memchached instance, which is used by the operator to keep some internal state.

```sh
kubectl get pods -n fluxNAME                         READY   STATUS    RESTARTS   AGE
flux-5dd6d54f5b-2gbws        1/1     Running   0          67s
memcached-5fd8f56fc5-qlpsc   1/1     Running   0          67s
```

## Retrieve SSH key via fluxctl

Flux connects to the Git repository using an SSH key. It generates the key on initial startup. You can retrieve the public SSH key via the ```fluxctl``` cli.

```sh
fluxctl identity --k8s-fwd-ns flux
```

## Add SSH key to the Deploy Keys of the Gitlab repository

Open Gitlab, navigate to your project, go to ```Settings -> Repository -> Deploy Keys``` and add the SSH key retrieved from fluxctl. Make sure you check Write access allowed.
Clone repository

## Use Git to clone the repository to your local machine and cd into the directory.

```sh
git clone https://gitlab.com/organicnz/gitops-flux-k8s
cd gitops-flux-k8s
```

## Create folders for manifest files

Let’s create two folders where our Kubernetes manifest file will be kept.

```sh
mkdir namespaces workloads
```

## Create Kubernetes manifest file for the demo namespace

We start by adding a Kubernetes namespace resource. Copy the snippet below and save it as ```demo-ns.yaml``` in the namespaces directory.

```sh
apiVersion: v1
kind: Namespace
metadata:
  labels:
    name: demo
  name: demo
```

## Create Kubernetes manifest file for the nginx deployment

Next, we will add a demo nginx deployment. Copy the snippet below and save it as ```nginx-deploy.yaml``` in the workloads directory.

```sh
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  namespace: demo
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
        - name: nginx
          image: nginx:1.14.2
          ports:
            - containerPort: 80
```

## Commit and push changes

Use Git to add the new files to the repository. Afterwards commit and push the changes to the Gitlab repository.

```sh
git add namespaces workloads
git commit -m 'Add demo resources'
git push
```

## Verify deployment

Verify the deployment by checking the status Kubernetes pods in the demo namespace. There should be two nginx pods running.

```sh
kubectl get pods -n demoNAME                                                   READY   STATUS    RESTARTS   AGE
nginx-deployment-6b6bc59c57-fd2w5              1/1     Running   0          57s
nginx-deployment-6b6bc59c57-vh8bh              1/1     Running   0          52s
```

## Summary

We deployed Flux and configured it to work with a Gitlab repository. Changes to the Git repository are deployed to the cluster automatically.

As you can see, there is no external client which needs access to the Kubernetes cluster which makes the process very secure and makes it perfect for deployments in highly regulated environments such as in the insurance and fintech industries.

Having a central repository to manage the Kubernetes manifest files makes the deployment easier to operate and removes complexity which comes with Kubernetes from the repository and pipelines of the services.

## Originally published at https://tomcode.com.